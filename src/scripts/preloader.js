document.addEventListener("DOMContentLoaded", function() {
    const transparentBackground = document.createElement("div");
    transparentBackground.style.position = "fixed";
    transparentBackground.style.top = "0";
    transparentBackground.style.left = "0";
    transparentBackground.style.width = "100%";
    transparentBackground.style.height = "100%";
    transparentBackground.style.background = "rgba(104,104,104, 0.8)";
    document.body.appendChild(transparentBackground);

    document.body.style.overflow = "hidden";
    setTimeout(function() {
        document.querySelector(".banter-loader").style.display = "none";
        document.body.removeChild(transparentBackground);
        document.body.style.overflow = "auto";
    }, 5000);
});